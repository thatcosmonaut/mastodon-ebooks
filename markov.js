const Markov = require('markov-strings')
const striptags = require('striptags')
const unescape = require('unescape')

const TootDownloader = require('./toot-downloader.js')

function stripAtsFromToots(toots) {
    return toots.map( (toot) => toot.replace( /@([\dA-Za-z\-\_]+) ?/g, '' ) );
}

function convertBRToEscapeLineBreak(toots) {
    var regex = /<br\s*[\/]?>/gi;

    return toots.map((toot) => toot.replace(regex, "\n"))
}

function stripHTMLTagsFromToots(toots)
{
    return toots.map((toot) => striptags(toot, ['<br>', '<a>']))
}

function unescapeToots(toots)
{
    return toots.map((toot) => unescape(toot))
}

function processToots(toots)
{
    return unescapeToots(stripAtsFromToots(stripHTMLTagsFromToots(toots.map((toot) => toot.content))))
}

exports.generateToot = async function(toots) {
    const markov = new Markov(processToots(toots))
    markov.buildCorpusSync()
    return markov.generateSentenceSync()
}