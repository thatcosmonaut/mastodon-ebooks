require('dotenv').load()
const async = require('async')
const fs = require('fs')
const TootDownloader = require('./toot-downloader.js')
const TootPoster = require('./toot-poster.js')
const markov = require('./markov.js')

async function main()
{
    var toots = []

    if(fs.existsSync('toots.json'))
    {
        toots = await TootDownloader.updateToots('toots.json')
    }
    else
    {
        toots = await TootDownloader.downloadAllToots('toots.json')
    }

    notReTootedToots = toots.filter((toot) => !toot.reblog)
    notSensitiveToots = notReTootedToots.filter((toot) => !toot.sensitive)

    var score = 0
    var finalToot = ""

    await async.doUntil(async () => {
        return await markov.generateToot(notSensitiveToots)
    }, (response) => {
        return response.score > 15 && response.string.length <= 500
    }, (err, response) => {
        finalToot = response.string
        //console.log(response)
        TootPoster.postToot(finalToot)
    })
}

main()