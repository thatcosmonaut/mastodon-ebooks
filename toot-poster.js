const Mastodon = require('mastodon')

var M = new Mastodon({
    access_token: process.env.OUTPUT_MASTODON_ACCESS_TOKEN,
    api_url: process.env.OUTPUT_MASTODON_API 
})

exports.postToot = async function (string)
{
    M.post('statuses', { status: string }, function(err, data, response) {
        console.log(`Posted status: ${string}`);
    });
}